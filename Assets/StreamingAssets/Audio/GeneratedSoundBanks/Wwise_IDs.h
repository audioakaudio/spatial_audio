/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHIMES = 2590078866U;
        static const AkUniqueID FOOTSTEP = 1866025847U;
        static const AkUniqueID MUSIC_BOX = 2781521992U;
        static const AkUniqueID ROOMTONE1 = 2410724393U;
        static const AkUniqueID ROOMTONE2 = 2410724394U;
        static const AkUniqueID XYLO = 184582855U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMBIENCE
        {
            static const AkUniqueID GROUP = 85412153U;

            namespace STATE
            {
                static const AkUniqueID AMBIENCE_OFF = 424387051U;
                static const AkUniqueID AMBIENCE_ON = 178275415U;
            } // namespace STATE
        } // namespace AMBIENCE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID OCCLUSION = 183903552U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID _3D_BUS = 2337546123U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID LARGEROOM = 187046019U;
        static const AkUniqueID SMALLROOM = 2933838247U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
