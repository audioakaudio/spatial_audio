﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_occlusion : MonoBehaviour
{
    public Vector3 OriginalPoint;
    public Vector3 OriginalPointLeft;
    public Vector3 OriginalPointRight;
    public Transform target;
    public float DistToPlayer;
   
 //   public float MaxAtt;


    // Update is called once per frame
    void Update()
    {
        OriginalPoint = transform.position;
        RaycastHit hit;
        RaycastHit hitL;
        RaycastHit hitR;
        Vector3 TargetDirection = target.position - transform.position + new Vector3(0, 1.5f, 0);
        DistToPlayer = Vector3.Distance(target.position, OriginalPoint);

        Physics.Raycast(OriginalPoint, TargetDirection, out hit, DistToPlayer);
        Physics.Raycast(OriginalPoint + new Vector3(1,0,0), TargetDirection - new Vector3(1, 0, 0), out hitL, DistToPlayer);
        Physics.Raycast(OriginalPoint - new Vector3(1, 0, 0), TargetDirection + new Vector3(1, 0, 0), out hitR, DistToPlayer);


        if (hit.collider.tag == "Occlusion" && hitL.collider.tag == "Occlusion" && hitR.collider.tag == "Occlusion")
        {

            Debug.DrawRay(OriginalPoint, TargetDirection, Color.red);
            Debug.DrawRay(OriginalPoint + new Vector3(1, 0, 0), TargetDirection - new Vector3(1, 0, 0), Color.red);
            Debug.DrawRay(OriginalPoint - new Vector3(1, 0, 0), TargetDirection + new Vector3(1, 0, 0), Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 3.0f);
        }

        else if (hit.collider.tag == "Occlusion" && hitL.collider.tag == "Occlusion")
        {

            Debug.DrawRay(OriginalPoint, TargetDirection, Color.red);
            Debug.DrawRay(OriginalPoint + new Vector3(1, 0, 0), TargetDirection - new Vector3(1, 0, 0), Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 2.0f);
        }

        else if (hit.collider.tag == "Occlusion" && hitR.collider.tag == "Occlusion")
        {

            Debug.DrawRay(OriginalPoint, TargetDirection, Color.red);
            Debug.DrawRay(OriginalPoint - new Vector3(1, 0, 0), TargetDirection + new Vector3(1, 0, 0), Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 2.0f);
        }
        else if (hit.collider.tag == "Occlusion" && hitR.collider.tag != "Occlusion" && hitL.collider.tag != "Occlusion")
        {

            Debug.DrawRay(OriginalPoint, TargetDirection, Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 1.0f);
        }
        else if (hitR.collider.tag == "Occlusion" && hit.collider.tag != "Occlusion" && hitL.collider.tag != "Occlusion")
        {

           
            Debug.DrawRay(OriginalPoint - new Vector3(1, 0, 0), TargetDirection + new Vector3(1, 0, 0), Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 1.0f);
        }
        else if (hitL.collider.tag == "Occlusion" && hit.collider.tag != "Occlusion" && hitR.collider.tag != "Occlusion" )
        {


            Debug.DrawRay(OriginalPoint + new Vector3(1, 0, 0), TargetDirection - new Vector3(1, 0, 0), Color.red);
            AkSoundEngine.SetRTPCValue("occlusion", 1.0f);
        }


if (hitL.collider.tag != "Occlusion" && hit.collider.tag != "Occlusion" && hitR.collider.tag != "Occlusion")
        {
            Debug.DrawRay(OriginalPoint, TargetDirection);
            AkSoundEngine.SetRTPCValue("occlusion", 0f);
        }
    }
}
