State Group	ID	Name			Wwise Object Path	Notes
	85412153	ambience			\Default Work Unit\ambience	

State	ID	Name	State Group			Notes
	0	None	ambience			
	178275415	ambience_on	ambience			
	424387051	ambience_off	ambience			

Custom State	ID	Name	State Group	Owner		Notes
	976571687	ambience_off	ambience	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\ambience		

Audio Bus	ID	Name			Wwise Object Path	Notes
	85412153	ambience			\Default Work Unit\Master Audio Bus\ambience	
	2337546123	3d_bus			\Default Work Unit\Master Audio Bus\3d_bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	187046019	largeroom			\Default Work Unit\Master Audio Bus\largeroom	
	2933838247	smallroom			\Default Work Unit\Master Audio Bus\smallroom	

Effect plug-ins	ID	Name	Type				Notes
	602546716	Room_Large	Wwise RoomVerb			
	1990128658	Room_Medium	Wwise RoomVerb			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

